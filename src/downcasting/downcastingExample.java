package downcasting;


class Flower{
	public void color() {
		System.out.println("flower is colorful");
	}
}

class Rose extends Flower{
	public void color() {
		System.out.println("Rose is red");
	}
	public void height() {
		System.out.println("Height is around 80cm");
	}
}

class Tulip extends Flower{
	public void color() {
		System.out.println("Tulip is yellow and pink");
	}
	public void season() {
		System.out.println("Tulip is summer flower");
	}
	
}		
public class downcastingExample {
	
	public static void main(String[] args) {
		
		//upcasting

		Flower flower1 = new Rose();
		flower1.color();
		Flower flower2 = new Tulip();
		flower2.color();
		
		//downcasting
		
		if(flower1 instanceof Rose) {
			Rose convertedRose = (Rose)flower1;
			convertedRose.height();
		}
		else if (flower1 instanceof Tulip) {
			Tulip convertedTulip = (Tulip) flower1;
			convertedTulip.season();
		}
		if(flower2 instanceof Rose) {
			Rose convertedRose = (Rose)flower2;
			convertedRose.height();
		}
		else if (flower2 instanceof Tulip) {
			Tulip convertedTulip = (Tulip) flower2;
			convertedTulip.season();
		}
		
		//downcasting with a method
		
		Flower testFlower1 = new Rose();
		Flower testFlower2 = new Tulip();
		checkFlower(testFlower1);
		checkFlower(testFlower2);
		
	}
	
	public static void checkFlower(Flower flower){
		if(flower instanceof Rose) {
			Rose convertedRose = (Rose)flower;
			convertedRose.height();
		}
		else if (flower instanceof Tulip) {
			Tulip convertedTulip = (Tulip) flower;
			convertedTulip.season();
		}
	}
}
