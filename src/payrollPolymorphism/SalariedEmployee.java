package payrollPolymorphism;

public class SalariedEmployee extends Employee{
	protected double basicSalary;

	public SalariedEmployee(String firstName, String lastName, String ssn, double basicSalary) {
		super(firstName, lastName, ssn);
		this.basicSalary = basicSalary;
	}

	public double getBasicSalary() {
		return basicSalary;
	}

	public void setBasicSalary(double basicSalary) {
		this.basicSalary = basicSalary;
	}
	@Override
	public String toString() {
		return String.format("slaried employee: %s\n%s: $%.2f", super.toString(),"basic salary", getBasicSalary());
	}
	@Override
	public double salary() {
		return getBasicSalary();
	}
}
