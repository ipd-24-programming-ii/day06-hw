package payrollPolymorphism;

public class CommissionEmployee extends Employee {
	 protected double sales;
	 protected double commission;
	 public CommissionEmployee(String firstName, String lastName, String ssn, double sales, double commission) {
		super(firstName, lastName, ssn);
		this.sales = sales;
		this.commission = commission;
	}
	public double getSales() {
		return sales;
	}
	public void setSales(double sales) {
		this.sales = sales;
	}
	public double getCommission() {
		return commission;
	}
	public void setCommission(double commission) {
		this.commission = commission;
	}
	@Override
	public String toString() {
		return String.format("commission employee: %s\n%s: $%.2f, %s: %.2f", super.toString(),"gross sale", getSales(),"commission rate", getCommission());
	}
	@Override
	public double salary() {
		return getSales()*getCommission();

	}
}
