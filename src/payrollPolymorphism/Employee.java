package payrollPolymorphism;

public abstract class Employee {
	protected String firstName;
	protected String lastName;
	protected String ssn;
	public Employee(String firstName, String lastName, String ssn) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.ssn = ssn;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getSsn() {
		return ssn;
	}
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	@Override
	public String toString() {
		return String.format("%s %s\n%s: %s", getFirstName(),getLastName(),"Scocial Security Number: ", getSsn());
	}
	public abstract double salary();
}
