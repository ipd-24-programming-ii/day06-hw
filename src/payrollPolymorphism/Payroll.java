package payrollPolymorphism;

public class Payroll {
	protected Employee[] employeeArray;
	
	public Payroll(Employee[] employeeArray) {
		this.employeeArray= employeeArray;		
	}
	
	public void paySalary() {
		for (int i = 0; i < employeeArray.length; i++) {
			System.out.println(employeeArray[i]);
			System.out.println("Salary need to pay: "+ employeeArray[i].salary());
			System.out.println();
		}
	}
}
