package payrollPolymorphism;

public class Company {

	public static void main(String[] args) {
		
		CommissionEmployee commissionEmployee= new CommissionEmployee("Lily", " Princess","111-11-1111",50000,0.4);
		HourlyEmployee hourlyEmployee= new HourlyEmployee("William", " Smith","111-11-2222",80,15.5);
		SalariedEmployee salariedEmployee= new SalariedEmployee("John", " Lond","111-11-3333",50000);
		HourlyEmployee hourlyEmployee2= new HourlyEmployee("Steven", " Large","111-11-4444",50,12.5);
		
		
		Employee[] payroll = new Employee[] {commissionEmployee, hourlyEmployee, salariedEmployee,hourlyEmployee2};
		Payroll employeeList = new Payroll (payroll);
		employeeList.paySalary();
		
	}
}
