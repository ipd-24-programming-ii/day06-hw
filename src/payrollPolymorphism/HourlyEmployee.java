package payrollPolymorphism;

public class HourlyEmployee extends Employee {
	protected double wage;
	protected double hours;
	public HourlyEmployee(String firstName, String lastName, String ssn, double wage, double hours) {
		super(firstName, lastName, ssn);
		this.wage = wage;
		this.hours = hours;
	}
	public double getWage() {
		return wage;
	}
	public void setWage(double wage) {
		this.wage = wage;
	}
	public double getHours() {
		return hours;
	}
	public void setHours(double hours) {
		this.hours = hours;
	}
	@Override
	public String toString() {
		return String.format("hourly employee: %s\n%s: $%.2f, %s: %.2f", super.toString(),"hourly wage", getWage(),"hours worked", getHours());
	}
	@Override
	public double salary() {
		return getWage()*getHours();
	}

}
